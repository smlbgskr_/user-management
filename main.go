package main

import (
	"log"
	"user-management/handlers"
	"user-management/models"
	"user-management/repositories"
	"user-management/services"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	// Connect DB
	dsn := "host=localhost user=postgres password=admin dbname=user_manag port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal(err)
		panic("Cannot connect to DB")
	}
	log.Println("Connected to Database...")

	// Migrating DB From Models
	db.AutoMigrate(&models.User{})

	// Repository
	userRepository := repositories.NewRepository(db)
	userService := services.NewService(userRepository)
	userHandler := handlers.NewUserHandler(userService)

	router := gin.Default()

	v1 := router.Group("/v1")
	v2 := router.Group("/v2")

	// Get All Data
	v2.GET("/users/", userHandler.GetAllUsers)

	// Get Data By ID
	v1.GET("/user/:id", userHandler.GetUserByID)

	// Create Data
	v1.POST("/user/create", userHandler.CreateUserHandler)

	// Update Data
	v1.PUT("/user/:id", userHandler.UpdateUserHandler)

	// Delete Data
	v1.DELETE("/user/:id", userHandler.DeleteUserByID)

	router.Run()
}
