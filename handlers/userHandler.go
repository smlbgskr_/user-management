package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"user-management/requests"
	"user-management/services"

	"github.com/gin-gonic/gin"
)

type userHandler struct {
	userServ services.UserService
}

type pageHandler struct {
	userpageServ services.UserService
}

func NewUserHandler(userService services.UserService) *userHandler {
	return &userHandler{userService}
}

func (handler *userHandler) CreateUserHandler(c *gin.Context) {
	var request requests.UserRequest

	err := c.ShouldBindJSON(&request)
	message := "Error"
	if err != nil {
		c.JSON(http.StatusBadRequest, message)
		fmt.Println(err)
		return
	}

	createUser, err := handler.userServ.Create(request)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Cannot Create User",
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": createUser,
	})
}

func (handler *userHandler) UpdateUserHandler(c *gin.Context) {
	// ID from param
	idParam := c.Param("id")

	// Convert string to int
	id, _ := strconv.Atoi(idParam)

	var request requests.UserRequest

	err := c.ShouldBindJSON(&request)
	message := "Error"
	if err != nil {
		c.JSON(http.StatusBadRequest, message)
		fmt.Println(err)
		return
	}

	updateUser, err := handler.userServ.Update(id, request)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Cannot Update User",
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": updateUser,
	})
}

func (handler *userHandler) GetUserByID(c *gin.Context) {
	// ID from param
	idParam := c.Param("id")

	// Convert string to int
	id, _ := strconv.Atoi(idParam)

	userById, err := handler.userServ.FindByID(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Could Not Get Data User By ID",
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": userById,
	})
}

func (handler *userHandler) DeleteUserByID(c *gin.Context) {
	// ID from param
	idParam := c.Param("id")

	// Convert string to int
	id, _ := strconv.Atoi(idParam)

	userById, err := handler.userServ.Delete(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Could Not Delete Data User",
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": userById,
	})
}

func (handler *userHandler) GetAllUsers(c *gin.Context) {
	users, err := handler.userServ.FindAll()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Could Not Get All Data User",
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": users,
	})
}

// func GetAllUsers(user *models.User, pagination *models.Pagination) (*[]models.User, error) {
// 	var users []models.User
// 	var db *gorm.DB

// 	offset := (pagination.Page - 1) * pagination.Limit
// 	queryBuider := db.Limit(pagination.Limit).Offset(offset)
// 	result := queryBuider.Model(&models.User{}).Where(user).Find(&users)
// 	if result.Error != nil {
// 		msg := result.Error
// 		return nil, msg
// 	}
// 	return &users, nil
// }

// func GeneratePaginationFromRequest(c *gin.Context) models.Pagination {
// 	// Initializing default
// 	//	var mode string
// 	limit := 2
// 	page := 1
// 	query := c.Request.URL.Query()
// 	for key, value := range query {
// 		queryValue := value[len(value)-1]
// 		switch key {
// 		case "limit":
// 			limit, _ = strconv.Atoi(queryValue)
// 			break
// 		case "page":
// 			page, _ = strconv.Atoi(queryValue)
// 			break
// 		}
// 	}
// 	return models.Pagination{
// 		Limit: limit,
// 		Page:  page,
// 	}

// }
