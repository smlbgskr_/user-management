package repositories

import (
	"user-management/models"

	"gorm.io/gorm"
)

type UserRepository interface {
	FindAll() ([]models.User, error)
	FindByID(ID int) (models.User, error)
	Create(userCreate models.User) (models.User, error)
	Update(userUpdate models.User) (models.User, error)
	Delete(userDelete models.User) (models.User, error)
}

type userRepo struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *userRepo {
	return &userRepo{db}
}

func (repo *userRepo) FindAll() ([]models.User, error) {
	var users []models.User

	err := repo.db.Find(&users).Error

	return users, err
}

func (repo *userRepo) FindByID(ID int) (models.User, error) {
	var user models.User

	err := repo.db.Find(&user, ID).Error

	return user, err
}

func (repo *userRepo) Create(user models.User) (models.User, error) {
	err := repo.db.Create(&user).Error

	return user, err
}

func (repo *userRepo) Update(userUpdate models.User) (models.User, error) {
	err := repo.db.Save(&userUpdate).Error

	return userUpdate, err
}

func (repo *userRepo) Delete(userDelete models.User) (models.User, error) {
	err := repo.db.Delete(&userDelete).Error

	return userDelete, err
}
