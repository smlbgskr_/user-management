package services

import (
	"user-management/models"
	"user-management/repositories"
	"user-management/requests"

	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	FindAll() ([]models.User, error)
	FindByID(ID int) (models.User, error)
	Create(userCreate requests.UserRequest) (models.User, error)
	Update(ID int, userUpdate requests.UserRequest) (models.User, error)
	Delete(ID int) (models.User, error)
}

type userService struct {
	uServRepo repositories.UserRepository
}

func NewService(repo repositories.UserRepository) *userService {
	return &userService{repo}
}

func (us *userService) FindAll() ([]models.User, error) {
	return us.uServRepo.FindAll()
}

func (us *userService) FindByID(ID int) (models.User, error) {
	return us.uServRepo.FindByID(ID)
}

func (us *userService) Create(userCreate requests.UserRequest) (models.User, error) {
	userMap := models.User{
		Name:     userCreate.Name,
		Username: userCreate.Username,
		Password: userCreate.Password,
	}

	userMap.Password, _ = HashPassword(userMap.Password)

	return us.uServRepo.Create(userMap)
}

func (us *userService) Update(ID int, userUpdate requests.UserRequest) (models.User, error) {
	userMap, err := us.FindByID(ID)

	userMap.Username = userUpdate.Username
	userMap.Name = userUpdate.Name
	userMap.Password, _ = HashPassword(userUpdate.Password)

	newUser, err := us.uServRepo.Update(userMap)

	return newUser, err
}

func (us *userService) Delete(ID int) (models.User, error) {
	user, err := us.FindByID(ID)

	userDelete, err := us.uServRepo.Delete(user)

	return userDelete, err

}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
