/*
 Navicat Premium Data Transfer

 Source Server         : postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 120011
 Source Host           : localhost:5432
 Source Catalog        : user_manag
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120011
 File Encoding         : 65001

 Date: 12/07/2022 11:24:27
*/


-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "username" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1, 'smlbgskr', 'admin', 'Samuel Bagaskara');
INSERT INTO "public"."users" VALUES (3, 'any', 'user', 'Anya');
INSERT INTO "public"."users" VALUES (2, 'vncdt', '123', 'Vincint');
INSERT INTO "public"."users" VALUES (5, 'lana', '123', 'ln');
INSERT INTO "public"."users" VALUES (7, 'ln', '123', 'lina');
INSERT INTO "public"."users" VALUES (8, 'opppp', '12345678', 'pppppo');
INSERT INTO "public"."users" VALUES (9, 'coba', 'hashpasword', 'apasih');
INSERT INTO "public"."users" VALUES (10, 'test', '$2a$14$wmzoOPtX/ZpZdNdWNtcuEOXUjuIwCILfJ0GSzDWuQgdPP1YhdEPRC', 'lagii');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 12, true);

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_username_key" UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
