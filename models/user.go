package models

// Table User
type User struct {
	ID       int    `gorm:"primaryKey"`
	Username string `gorm:"type:varchar(100);unique;not null"`
	Password string `gorm:"type:varchar(100);not null"`
	Name     string `gorm:"type:varchar(100);not null"`
}
