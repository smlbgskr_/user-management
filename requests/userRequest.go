package requests

type UserRequest struct {
	Username string `json:"username" binding:"required,min=3"`
	Password string `json:"password" binding:"required,min=7"`
	Name     string `json:"name" binding:"required,min=3"`
}
